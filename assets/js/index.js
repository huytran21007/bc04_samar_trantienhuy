$(document).ready(() => {
  const wow_run = () => {
    new WOW().init();
  };
  const counter_up_run = () => {
    const counter_config = {
      delay: 10,
      time: 3000,
    };
    $(".counter").counterUp(counter_config);
  };
  const light_gallery_run = () => {
    let lg_thumbs_list = $(".lg__thumbs-list")[0];
    const lg_config = {
      selector: ".btn-view",
      plugins: [lgZoom, lgThumbnail, lgAutoplay, lgFullscreen, lgShare],
      animateThumb: false,
      alignThumbnails: "left",
      loop: true,
      allowMediaOverlap: true,
      toggleThumb: true,
      showZoomInOutIcons: true,
      actualSize: false,
      exThumbImage: "data-exthumbimage",
    };

    lightGallery(lg_thumbs_list, lg_config);
  };
  const testimonial_swiper_run = () => {
    let common_config = {
      spaceBetween: 10,
      autoplay: true,
      loop: true,
      speed: 3000,
    };
    // declare config for testimonial thumb swiper
    let testimonial_thumbs_swiper_config = {
      ...common_config,
      slidesPerView: 3,
      centeredSlides: true,
      freeMode: true,
    };

    // declare config for testimonial thumb swiper
    let testimonial_comment_swiper_config = {
      ...common_config,
      pagination: {
        el: ".swiper-pagination",
        clickable: true,
      },
    };

    // initialize thumb swiper
    const testimonial_thumbs_swiper = new Swiper(
      ".testimonial__thumbs",
      testimonial_thumbs_swiper_config
    );

    // initialize comment swiper
    const testimonial_comment_swiper = new Swiper(".testimonial__comment", {
      ...testimonial_comment_swiper_config,
      thumbs: { swiper: testimonial_thumbs_swiper },
    });
  };

  const owl_carousel_run = () => {
    const owl_carousel_config = {
      items: 3,
      margin: 30,
    };

    $(".owl-carousel").owlCarousel(owl_carousel_config);
  };

  // call and run main function
  wow_run();
  counter_up_run();
  light_gallery_run();
  testimonial_swiper_run();
  owl_carousel_run();
});
